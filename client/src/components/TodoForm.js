import React, { useEffect } from "react";
import { Field, reduxForm } from 'redux-form';
import { useDispatch, useSelector } from "react-redux";
import DeleteIcon from '@material-ui/icons/Delete';
import fetchNotesWatcher from "../store/actions/fetchNotesAction";
import addNoteWatcher from "../store/actions/addNoteAction";
import deleteNoteWatcher from "../store/actions/deleteNoteAction";



const TodoForm = () => {
  const dispatch = useDispatch();

  useEffect(() => {
    dispatch(fetchNotesWatcher());
  }, [dispatch]);


  const getAllNotes = useSelector(state => state.fetchNotesReducer.notes && state.fetchNotesReducer.notes.data);

  console.log("getAllNotes", getAllNotes);

  const handleDelete = id =>{
    console.log("Deletes  ldkjklmdkm")
    dispatch(deleteNoteWatcher(id));
  }
  
  const handleSubmit = e =>{
    e.preventDefault();
    dispatch(addNoteWatcher());
  }

  return (
    <>
    <form onSubmit={handleSubmit}>
          <Field
            name="note"
            component="input"
            type="text"
            placeholder="Add note"
          />
      <div>
        <button type="submit">
          Add
        </button>
      </div>
    </form>
      <div>
        {getAllNotes && getAllNotes.map(item => {
          return <div>{item.title} <span><DeleteIcon onClick={e => handleDelete(item.id)} ></DeleteIcon></span></div>
        })}
      </div>
    </>
  );
};


export default reduxForm({
  form: 'TodoForm',
})(TodoForm);

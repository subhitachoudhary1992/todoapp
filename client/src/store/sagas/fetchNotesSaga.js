import { call,put,takeEvery } from 'redux-saga/effects'
import { TODO_FETCH_NOTES } from '../../types/index';
import { getCall } from '../../utils/apiSignature';

function* fetchNotesWorker(){
    console.log('sdfsf');
    const {response,error} = yield call(getCall,`https://localhost:8001/todos`);
    if (response){
        yield put({
            type : TODO_FETCH_NOTES.FETCH_NOTES_SUCCESS,
            payload : response
        })
    }else if(error){
        yield put({
            type : TODO_FETCH_NOTES.FETCH_NOTES_FAILURE,
            payload : error
        })
    }
}

export default function* fetchNotesWatcher() {
  console.log("inside saga")
    yield takeEvery(TODO_FETCH_NOTES.FETCH_NOTES_REQUEST, fetchNotesWorker);
  }
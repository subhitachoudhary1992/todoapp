import { call,select,put,takeLatest } from 'redux-saga/effects'
import { TODO_ADD_NOTE } from '../../types/index';
import { postCalls } from '../../utils/apiSignature';

const todoFormData = state => state.form.TodoForm.values.note;

function* addNoteWorker() {
    console.log('Success', todoFormData)
    const clientForm = yield select(todoFormData);
    console.log('client form ::', clientForm)
    const { response, error } = yield call(
      postCalls,
      `https://localhost:8001/todos/`,
      clientForm,
    );
    if (response) {
      console.log('Success')
    //   const data = {
    //     msg: 'Client Created !',
    //     flagType: 'success',
    //   };
      yield put({
        type : TODO_ADD_NOTE.ADD_NOTE_SUCCESS,
        payload : response
    });
    } else {
      const data = {
        msg: error.response.errorMessage,
        flagType: 'error',
      };
      yield put({
        type : TODO_ADD_NOTE.ADD_NOTE_FAILURE,
        payload : data
    });
    }
  }


export default function* addNoteWatcher() {
  console.log("inside saga")
    yield takeLatest(TODO_ADD_NOTE.ADD_NOTE_REQUEST, addNoteWorker);
  }
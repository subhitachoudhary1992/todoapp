import { all } from 'redux-saga/effects';
import fetchNotesWatcher from './fetchNotesSaga';
import addNoteWatcher from './addNoteSaga';
import deleteNoteWatcher from './deleteNoteSaga';

export default function* rootSaga() {
  yield all([
    fetchNotesWatcher(),
    addNoteWatcher(),
    deleteNoteWatcher(),
  ]);
}

import { TODO_DELETE_NOTE } from "../../types/index";

const deleteNoteWatcher = id => {
    console.log('notes',id)
  return {
    type: TODO_DELETE_NOTE.DELETE_NOTE_REQUEST,
    id
  };
};

export default deleteNoteWatcher;
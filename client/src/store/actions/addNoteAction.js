import { TODO_ADD_NOTE } from "../../types/index";

const addNoteWatcher = notes => {
  return {
    type: TODO_ADD_NOTE.ADD_NOTE_REQUEST,
    notes
  };
};

export default addNoteWatcher;
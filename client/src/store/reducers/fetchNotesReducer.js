import { TODO_FETCH_NOTES } from '../../types/index'

const fetchNotesReducer = (state = {},action) =>{
    console.log('action reducer', action)
    switch (action.type) {
        case TODO_FETCH_NOTES.FETCH_NOTES_REQUEST:
            console.log('inside fetch note', state)
            return{
                ...state,
                loading : true
            }
        
        case TODO_FETCH_NOTES.FETCH_NOTES_SUCCESS:
            return {
                loading : false,
                notes : action.payload,
                error : ''
            }
        
        case TODO_FETCH_NOTES.FETCH_NOTES_FAILURE:
            return{
                loading : false,
                notes : [],
                error : action.payload
            }
    
        default:
            return state;
    }
} 

export default fetchNotesReducer;
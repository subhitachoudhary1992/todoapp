import { TODO_ADD_NOTE } from '../../types/index'

const addNoteReducer = (state = {},action) =>{
    console.log('add note Reducer', action)
    switch (action.type) {
        case TODO_ADD_NOTE.ADD_NOTE_REQUEST:
            return{
                ...state,
                loading : true
            }
        
        case TODO_ADD_NOTE.ADD_NOTE_SUCCESS:
            return {
                ...state,
                loading : false,
                error : ''
            }
        
        case TODO_ADD_NOTE.ADD_NOTE_FAILURE:
            return{
                loading : false,
                notes : [],
                error : action.payload
            }
    
        default:
            return state;
    }
} 

export default addNoteReducer;
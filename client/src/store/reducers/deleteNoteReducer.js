import { TODO_DELETE_NOTE } from '../../types/index'

const deleteNoteReducer = (state = {},action) =>{
    console.log('delete note Reducer', action)
    switch (action.type) {
        case TODO_DELETE_NOTE.DELETE_NOTE_REQUEST:
            return{
                ...state,
                loading : true
            }
        
        case TODO_DELETE_NOTE.DELETE_NOTE_SUCCESS:
            return {
                ...state,
                loading : false,
                error : ''
            }
        
        case TODO_DELETE_NOTE.DELETE_NOTE_FAILURE:
            return{
                loading : false,
                notes : [],
                error : action.payload
            }
    
        default:
            return state;
    }
} 

export default deleteNoteReducer;